#!/usr/bin/env bash
export DEBIAN_FRONTEND=noninteractive
export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_TYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
locale-gen en_US en_US.UTF-8
dpkg-reconfigure locales

apt-get update
apt-get upgrade

echo "=================================================="
echo "INSTALLING NGINX"
echo "=================================================="
apt-get -y install nginx

if ! [ -L /home/users/magento ]; then
  rm -rf /home/users/magento
  mkdir -p /home/users/magento/logs
else
  mkdir -p /home/users/magento/logs
fi

if ! [ -L /home/users/xss ]; then
  rm -rf /home/users/xss
  mkdir -p /home/users/xss/logs
else
  mkdir -p /home/users/xss/logs
fi

if ! [ -L /home/users/obj ]; then
  rm -rf /home/users/obj
  mkdir -p /home/users/obj/logs
else
  mkdir -p /home/users/obj/logs
fi

if ! [ -L /home/users/iframe ]; then
  rm -rf /home/users/iframe
  mkdir -p /home/users/iframe/logs
else
  mkdir -p /home/users/iframe/logs
fi

ln -fs /vagrant/source/obj /home/users/obj/www
ln -fs /vagrant/source/iframe /home/users/iframe/www
ln -fs /vagrant/httpdocs /home/users/magento/www
ln -fs /vagrant/source/xss /home/users/xss/www

cp /vagrant/source/conf/magento /etc/nginx/sites-available/
cp /vagrant/source/conf/xss /etc/nginx/sites-available/
cp /vagrant/source/conf/iframe /etc/nginx/sites-available/
cp /vagrant/source/conf/obj /etc/nginx/sites-available/

ln -s /etc/nginx/sites-available/iframe /etc/nginx/sites-enabled/iframe
ln -s /etc/nginx/sites-available/magento /etc/nginx/sites-enabled/magento
ln -s /etc/nginx/sites-available/xss /etc/nginx/sites-enabled/xss
ln -s /etc/nginx/sites-available/obj /etc/nginx/sites-enabled/obj

service nginx restart
echo "=================================================="
echo "INSTALLING PHP-FPM"
echo "=================================================="
apt-get -y update
apt-get -y install php5 php5-mhash php5-mcrypt php5-curl php5-cli php5-mysql php5-gd php5-intl php5-xsl php5-xdebug php5-fpm

cp /vagrant/source/conf/20-xdebug.ini /etc/php5/mods-available/xdebug.ini
if ! [ -L /etc/php5/fpm/conf.d/20-xdebug.ini ]; then
  ln -s /etc/php5/mods-available/xdebug.ini /etc/php5/fpm/conf.d/20-xdebug.ini 
fi

service php5-fpm restart
apt-get -y install ntp


echo "=================================================="
echo "INSTALLING MYSQL"
echo "=================================================="
apt-get -q -y install mysql-server-5.5

echo "=================================================="
echo "INSTALLING MYSQL MAGENTO DATABASE"
echo "=================================================="

mysql -u root -e "DROP DATABASE IF EXISTS magento"
mysql -u root -e "CREATE DATABASE IF NOT EXISTS magento"
mysql -u root -e "GRANT ALL PRIVILEGES ON magento.* TO 'magento'@'localhost' IDENTIFIED BY 'password'"
mysql -u root -e "FLUSH PRIVILEGES"


echo "=================================================="
echo "CLEANING..."
echo "=================================================="
apt-get -y autoremove
apt-get -y autoclean


echo "Import Sample database..."
mysql -u root magento < /vagrant/source/sql/magento_sample.sql



mysql -u root -e "UPDATE magento.core_config_data SET value = 'http://magento-edu.local/' WHERE core_config_data.path = 'web/unsecure/base_url'"
mysql -u root -e "UPDATE magento.core_config_data SET value = 'http://magento-edu.local/' WHERE core_config_data.path = 'web/secure/base_url'"
mysql -u root -e "UPDATE magento.core_config_data SET value = 'magento-edu.local' WHERE core_config_data.path like 'web/cookie/cookie_domain'"

chown -R www-data:www-data /home/users/

echo "done."

echo "=================================================="
echo "============= INSTALLATION COMPLETE =============="
echo "=================================================="
